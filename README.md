# Manual for setting up IntroDH17.box VM 

Given all the different OS and software versions used by students, we have decided to package some tools necessary to run our scripts, libraries, and tools in a virtual machine and ship it in a Vagrant box, the `IntroDH17.box`. This keeps OS specific helpdesking and troubleshooting to a minimum. All you have to do to get the `IntroDH17.box` is to install VirtualBox and Vagrant and initialise the box. Please find more information below. Due to the lack of a secure password / username, please do not save any sensitive information on your virtual machine. `IntroDH17.box` is a learning environment and not a permament solution for virtualising your research. If you would like to build a virtual machine that is similar to `IntroDH17.box`, but safer, we are happy to give some hints and advice.

## What is in the `IntroDH17.box`

### Download VirtualBox

Find VirtualBox [here](https://www.virtualbox.org).

### Download Vagrant

Find Vagrant [here](https://www.vagrantup.com).

### Download `IntroDH17.box`

Download the `IntroDH17.box` from [here](http://nlp.dainst.org/dh17/IntroDH17.box)

**Note**: it's a very heavy file (3.6 GB)! Download might be slow and it can wreck your internet data plan. Your best option is to download it from the University network.

### Setting up `IntroDH17.box`

You should get familiar with the terminal / command line interface of your operating system. For more information see: [Linux](http://askubuntu.com/questions/38162/what-is-a-terminal-and-how-do-i-open-and-use-it), [Mac](http://blog.teamtreehouse.com/introduction-to-the-mac-os-x-command-line), [Windows](http://www.digitalcitizen.life/7-ways-launch-command-prompt-windows-7-windows-8).

1. Create a folder for `IntroDH17.box` and move `IntroDH17.box` there and switch to it
E.g. 
```
mkdir ~/somefolder/DHLeipzig/vagrant_boxes
cd ~/somefolder/DHLeipzig/vagrant_boxes
```

2. Move `IntroDH17.box` to the new folder
3. Add `IntroDH17.box` to Vagrant: `vagrant box add IntroDH17 IntroDH17.box`
4. Initialise the IntroDH17.box: `vagrant init IntroDH17`
5. Start it for the first time:: `vagrant up`
6. Halt the IntroDH17.box: `vagrant halt`
7. Modify vagrantfile so it starts with a GUI and activate port-forwarding (lines you have to add). Find and uncomment in the vagrantfile (open with an editor of your choice):
8. 
```
 config.vm.network "forwarded_port", guest: 3333, host: 3333 # To use OpenRefine on your host machine
 config.vm.network "forwarded_port", guest: 8787, host: 8787 # To use RStudio on your host machine
 config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024" # Uncomment this and change it to alter the RAM of your VM
end
```

8. If you want and if your host and guest machines are too slow, you can augment the amount of RAM memory and number of cpu of the VM, following [these](https://www.vagrantup.com/docs/virtualbox/configuration.html#vboxmanage-customizations) instructions. E.g. by replacing the lines before with:

```
config.vm.provider "virtualbox" do |vb|
  vb.gui = true
  # give it roughly 1/2 your total RAM;
  # if you have 8gb give it 4
  vb.memory = "4096" 
  
  #again, half the cpus
  vb.cpus = 2
end

```

10. Start again: `vagrant up` 
11. Enjoy all the pre-installed tools and libraries
12. If you want to delete your VM, type `vagrant destroy`


